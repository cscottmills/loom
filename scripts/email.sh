#! /bin/bash
BLUE='\033[0;36m'
NC='\033[0m'
echo -e "\n${BLUE}building site...${NC}\n"
JEKYLL_ENV=email bundle exec jekyll build
echo -e "\n${BLUE}inlining styles...${NC}"
juice --preserve-important true --remove-style-tags false --web-resources-images false _site/email/index.html _site/email/index.html
pbcopy < _site/email/index.html
echo -e "\n${BLUE}html copied to clipboard 🌿${NC}\n"
