#! /bin/bash
BLUE='\033[0;36m'
NC='\033[0m'
clear
echo -e "\n${BLUE}building site...${NC}\n"
JEKYLL_ENV=production bundle exec jekyll build --config _config.yml,_config-web.yml
echo -e "\n${BLUE}carrying files to server...${NC}\n"
# Replace user, IP and path below to the details for your server
# The 'n' flag does a dry-run without actually moving any files. When you're *sure* everything is right, remove the 'n' from '-anv'.
# rsync -anv _site/ user@0.0.0.0:/path/to/html/ --delete
echo -e "\n${BLUE}site published!${NC}\n"
