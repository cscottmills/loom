---
layout: post
title: Subscribe
---

Here's the markup for a subscription form, which you can hook up to whatever newsletter service you use.

{% include form.html %}
